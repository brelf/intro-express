import request from 'supertest';

import { app, bootstrap } from '../src/app';

describe('our app', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  // código pt. 1
  test('GET / returns a "hello world"', async () => {
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });

  // código pt. 2
  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt.2
  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 3
  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  // código pt. 4
  test('DELETE /projects/:slug delete a project', async () => {
    let reqPOST = await request(app)
      .post('/projects')
      .send({ name: 'youtubiuu' });

    expect(reqPOST.body.project).toBeDefined();
    expect(reqPOST.body.project.slug).toBe('youtubiuu');
      
    let reqDEL = await request(app).delete('/projects/youtubiuu');
    
    expect(reqDEL.status).toBe(200);
  });

  // código pt. 5
  test('DELETE /projects/:slug/boards/:name delete a board', async () => {
    const project = { name: 'twitch' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'asmodaiTV';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);

    let reqDEL = await request(app).delete('/projects/twitch/boards/asmodaiTV');

    expect(reqDEL.status).toBe(200);
    expect(reqDEL.body.project.boards.length).toBe(0);
  });

  // código pt. 6
  test('POST /projects/:slug/boards/:name/tasks Add a task to a board', async () => {
    const project = { name: 'twitch' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'asmodaiTV';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);

    const taskID = 'watch';
    let reqPOST = await request(app)
      .post('/projects/twitch/boards/asmodaiTV/tasks')
      .send({ taskID });

    expect(reqPOST.status).toBe(200);
    expect(reqPOST.body.project.boards[0].tasks.length).toBe(1);
    expect(reqPOST.body.project.boards[0].tasks[0]).toBe('watch');
  });
  
  // código pt. 7
  test('DELETE /projects/:slug/boards/name/tasks/:id Delete a task from a board', async () => {
    const project = { name: 'twitch' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'asmodaiTV';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);

    const taskID = 'watch';
    let reqPOST = await request(app)
      .post('/projects/twitch/boards/asmodaiTV/tasks')
      .send({ taskID });

    expect(reqPOST.status).toBe(200);
    expect(reqPOST.body.project.boards[0].tasks.length).toBe(1);
    expect(reqPOST.body.project.boards[0].tasks[0]).toBe('watch');

    let reqDEL = await request(app).delete('/projects/twitch/boards/asmodaiTV/tasks/watch');

    expect(reqDEL.status).toBe(200);
    expect(reqDEL.body.project.boards[0].tasks.length).toBe(0);

  });

});
