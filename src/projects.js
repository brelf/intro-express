import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { setupDB } from './db';

const express = require('express');
const router = express.Router();

router.use(cors());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.post('/', async (req, res) => {
    const { name } = req.body;
    const db = await setupDB();
    const slug = name.toLowerCase();

    const project = {
        slug,
        boards: [],
    };
        
    const existingProject = await db.collection('projects').findOne({ slug });
    
    if (existingProject) {
        res.status(400).json({ error: `project ${slug} already exists` });
    } else {
        await db.collection('projects').insertOne(project);
        res.json({ project });
    }
});

const findProject = async (req, res, next) => {
    const { slug } = req.params;
    const db = await setupDB();
    const project = await db.collection('projects').findOne({ slug });

    if (!project) {
        res.status(400).json({ error: `project ${slug} does not exist` });
    } else {
        req.body.project = project;
        next();
    }
};

router.get('/:slug', findProject, async (req, res) => {
    const { project } = req.body;
    res.json({ project });
});

router.post('/:slug/boards', findProject, async (req, res) => {
    const { name, project } = req.body;
    const db = await setupDB();
    const board = { name, tasks: [] };
    project.boards.push(board);

    await db
        .collection('projects')
        .findOneAndReplace({ slug: project.slug }, project);

    res.json({ board });
});

router.delete('/:slug', findProject, async (req, res) => {
    const { project } = req.body;
    const db = await setupDB();

    await db.collection('projects').deleteOne({ slug: project.slug });
        
    res.json({ project });
    res.status(200);
});

router.delete('/:slug/boards/:name', findProject, async (req, res) => {
    const { project } = req.body;
    const { name } = req.params;    
    const db = await setupDB();
    const previousLength = project.boards.length;

    for (let i = 0; i < project.boards.length; i++) {
        if (project.boards[i].name === name) {
            project.boards.splice(i, 1);
        }
    }

    if (previousLength === project.boards.length) {
        res.status(400).json({ error: `board with name ${name} does not exist`});
    } else {
        await db
            .collection('projects')
            .findOneAndReplace({ slug: project.slug }, project);

        res.json({ project });
        res.status(200);
    }
})

router.post('/:slug/boards/:name/tasks', findProject, async (req, res) => {
    const { project, taskID } = req.body;
    const { name } = req.params;
    const db = await setupDB();

    for (let i = 0; i < project.boards.length; i++) {
        if (project.boards[i].name === name) {
            project.boards[i].tasks.push(taskID);
        }
    }

    await db
        .collection('projects')
        .findOneAndReplace({ slug: project.slug }, project);

    res.json({ project });
    res.status(200);
});

router.delete('/:slug/boards/:name/tasks/:id', findProject, async (req, res) => {
    const { project } = req.body;
    const { name, id } = req.params;    
    const db = await setupDB();
    let success = false;

    for (let i = 0; i < project.boards.length; i++) {
        if (project.boards[i].name === name) {
            const previousLength = project.boards[i].tasks.length;
            success = true;
            for (let j = 0; j < project.boards[i].tasks.length; j++) {
                if (project.boards[i].tasks[j] === id) {
                    project.boards[i].tasks.splice(j, 1);
                }
            }
            if (previousLength === project.boards[i].tasks.length) {
                success = false;
            }
            break;
        }
    }

    if (!success) {
        res.status(400).json({ error: `board with name ${name} or task with id ${id} does not exist`});
    } else {
        await db
            .collection('projects')
            .findOneAndReplace({ slug: project.slug }, project);

        res.json({ project });
        res.status(200);
    }
})

module.exports = router